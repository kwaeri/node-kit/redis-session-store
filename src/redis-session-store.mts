/**
 * SPDX-PackageName: kwaeri/redis-session-store
 * SPDX-PackageVersion: 1.0.3
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import {
    ClientSession,
    SessionStore,
    BaseSessionStore,
    SessionBits
} from '@kwaeri/session-store';
import { Configuration } from '@kwaeri/configuration';
import { kdt } from '@kwaeri/developer-tools';
import { createClient, RedisClientType } from 'redis';
import debug from 'debug';


// DEFINES
const _ = new kdt(),
      DEBUG = debug( 'nodekit:redis-session-store' );


/**
 * Memcached Session Store
 *
 * Manages a 'store' object, where each memcached key is a session
 * named after its 'ID'. For each key, an object that is the session,
 * and all of its associated information, is stored within a JSON string.
 */
export class RedisSessionStore implements SessionStore {
    /**
     * @var { string }
     */
    public version: string;

    /**
     * @var { string }
     */
    public type: string;

    /**
     * @var { any }
     */
    protected configuration: any;

    /**
     * @var { Configuration }
     */
    private redisConfiguration: Configuration;

    /**
     * @var { any }
     */
    private redisConf?: any;

    /**
     * @var { any }
     */
    private store?: RedisClientType;

    /**
     * Class constructor
     *
     * @param { any } store The store interface. In this case, a filesystem IO interface
     * @param { any } configuration The session store configuration
     *
     * @returns { void }
     */
    constructor( configuration: any ) {

        // Organize all of the uncertainty:
        this.version     = ( configuration && configuration.version ) ? configuration.version : null,
        this.type        = ( configuration && configuration.type ) ? configuration.type : "Database";

        // Pack the store interface into the configuration:
        //configuration = _.set( configuration, 'store', new Memcached( _.get( configuration, 'locations', 'localhost:11211' ) ) );
        //this.store       = new createClient( { url: _.get( configuration, 'url', 'redis://nodekit:TestPass777@localhost:6379' ) } );

        // This is just the session configuration from conf/
        this.configuration = configuration;

        this.redisConfiguration = new Configuration( 'conf', `redis.${configuration.environment}.json` );

        // Call the parent class:
        //super( configuration );
    }


    /**
     * Gets the redis server configuration
     *
     * @param { void }
     *
     * @returns { Promise<Configuration> } the promise for a {@link Configuration} object
     */
    private async getConf(): Promise<any> {
        if( this.redisConf === undefined ) {
            const redisConf = await this.redisConfiguration.get();

            if( !redisConf )
                    return Promise.reject( new Error( `[REDIS_SESSION_STORE][GET_CONF] There was an issue reading the Redis configuration. ` ) );

            DEBUG( `Redis configuration successfully fetched:` );
            DEBUG( redisConf );

            this.redisConf = redisConf;
        }

        // Otherwise, return the conf
        return Promise.resolve( this.redisConf );
    }


    /**
     * Get the store; If store is undefined` then attempt to create it, and reject the
     * promise in the event that this method fails.
     *
     * @returns { Promise<any> } the promise for a {@link RedisClient} object
     */
    private async getClient(): Promise<any> {
        try{
            if( !this.store ) {
                DEBUG( `Get Redis client` );

                const {
                    username,
                    password,
                    host,
                    port,
                    secure,
                    dbNumber
                } = await this.getConf();

                /**
                 * Build something akin to:
                 * `redis[s]://[[username][:password]@][host][:port][/db-number]`
                 *
                 * If a server has a user and password set via ACL, and they're
                 * provided - properly format the full connection string as per
                 * the template above.
                 *
                 * However, there are several other possibilities that require
                 * special attention:
                 *
                 * * If a server has requirepass set, and its provided - properly
                 *   format the full connection string like above, leveraging the
                 *   `default` user.
                 * * If a server has no password requirement, but provides a
                 *   username - properly format the connection string without a
                 *   password.
                 * * If a server is configured as open with no users configured,
                 *   ignore the authentication portion of the connection string
                 *   since the server will default to the `default` users with
                 *   out a password.
                 *
                 */
                const authenticate = ( username && username.length ) ? true : false,
                      withPassword = ( password && password.length ) ? true : false,
                      securely = ( secure ) ? true : false,
                      asperand = ( authenticate ) ? "@" : "",
                      authentication = ( authenticate && withPassword ) ? `${username}:${password}` : ( withPassword ) ? `default:${password}` : ( authenticate ) ? `username` : "",
                      location = `${host}:${port&&port.length?port.toString():"6379"}${dbNumber&&dbNumber.length?`/${dbNumber}`:""}`,
                      url = `redis${securely?"s":""}://${authentication}${asperand}${location}`;

                DEBUG( `Authenticating [%s] with  password [%s]`, authenticate, withPassword );
                DEBUG( `Connect to redis server at %s`, url );

                // Get the Redis client
                const store = createClient( { url } );

                if( !store )
                    return Promise.reject( new Error( `There was an issue getting the Redis client` ) );

                // Configure logging
                store.on( "error", error => console.error( `Redis Client Error: ${error}` ) );

                // Make a connection to the server
                await store.connect();

                // Configure an exit strategy
                this.onExit();

                this.store = store as RedisClientType;
            }

            // And return the store
            return Promise.resolve( this.store! );
        }
        catch( error ) {
            DEBUG( `${error}` );

            return Promise.reject( `${error}` );
        }
    }

    /**
     * Configures listeners for most of the important [to us] events that would
     * signal a need to gracefully close the connection to our Redis server
     *
     * @param { void } `none`
     *
     * @returns { void } `void`
     */
    private onExit(): void {
        // Configure exit strategy:
        process.on(
            'exit',
            () => {

                this.store?.quit();
                DEBUG( 'Closing Redis client connection' );
            }
        );
        process.on(
            'SIGINT',
            () => {

                this.store?.quit();
                DEBUG( 'Closing Redis client connection' );
            }
        );
        process.on(
            'SIGTERM',
            () => {

                this.store?.quit();
                DEBUG( 'Closing Redis client connection' );
            }
        );
    }


    /**
     * A method to read a key's stored hash (all fields) on the redis server
     *
     * @param { string } id The id of the session to use as a key value to read from
     *
     * @return { Promise<ClientSession|boolean|null> } the promise for a {@link ClientSession}, or `null`.
     */
    private async read( id: string ): Promise<ClientSession|null> {
        try {
            DEBUG( `Read the session '%s'`, id );

            // Fetch the client
            const store = await this.getClient();

            // Always prepend 'session:' to every key, it's how we manage ACL in a
            // finely tuned manner:
            const read = await store.get( `session:${id}` );

            DEBUG( `'read' result: [%s]`, read );

            if( read && read.length ){
                const session = JSON.parse( read );
                return Promise.resolve( session );
            }
            else
                return Promise.resolve( null );
        }
        catch( exception ) {
            DEBUG( `[READ] Error: %o`, exception );

            return Promise.reject( `[REDIS_SESSION_STORE]: There was an issue reading the session '${id}' on the redis server: ${exception}. ` );
        }
    }


    /**
     * A method to read the value of a field from a key's hash on the redis server
     *
     * @param { string } id The id of the session to use as a key value to read from
     * @param { string } name the name of the session field
     *
     * @return { Promise<any|null> } the promise for a {@link ClientSession}, or `null`.
     */
    private async readField( id: string, name: string ): Promise<any|null> {
        try {
            DEBUG( `Read field '%s' in the session '%s'`, name, id );

            // Fetch the client
            const store = await this.getClient();

            // Always prepend 'session:' to every key, it's how we manage ACL in a
            // finely tuned manner:
            const read = await store.hGet( `session:${id}`, name ) as string;

            if( read !== undefined && read !== null && (
                ( _.type( read ) === 'string' && read.length ) ||
                //( _.type( read ) === 'number' ) ||
                (_.type( read ) === "boolean" )
            ) )
                return Promise.resolve( read );
            else
                return Promise.resolve( null );
        }
        catch( exception ) {
            DEBUG( `[READ] Error: %o`, exception );

            return Promise.reject( `[REDIS_SESSION_STORE]: There was an issue reading the value of field '${name}' in session '${id}' on the redis server: ${exception}. ` );
        }
    }


    /**
     * A method to write a session as a key's hash on the redis server
     *
     * @param { any } id The name of the session to use as a key
     * @param { any } session The session data to store with the new key
     *
     * @returns { Promise<boolean> } the promise for a `boolean`
     */
    private async write( id: string, sessionBits: SessionBits, expires: number = 900 ): Promise<boolean> {
        try {
            DEBUG( `Write session '%s' with expiry of '%s`, id, sessionBits.expires );

            // Fetch the client
            const store = await this.getClient();

            // Write the data as a json string
            const write = await store.set( `session:${id}`, JSON.stringify( sessionBits ), { PX: sessionBits.expires } );

            DEBUG( `'write' result: [%s]`, write );

            if( write && write.length /*&& setExpiry*/ )
                return Promise.resolve( true );
            else
                return Promise.resolve( false );
        }
        catch( exception ) {
            DEBUG( `[WRITE] Error: %o`, exception );

            return Promise.reject( `[REDIS_SESSION_STORE]: There was an issue writing the session '${id}' on the redis server: ${exception}. ` );
        }
    }


    /**
     * A method to write a value to a field of a key hash on the redis server
     *
     * @param { string } id The name of the session as a key
     * @param { string } name the name of the session field
     * @param { any } value The hash field value/data
     *
     * @returns { Promise<boolean> } the promise for a `boolean`
     */
    private async writeField( id: string, name: string, value: any, expires: number = 900 ): Promise<boolean> {
        try {
            DEBUG( `Set field '%s' to '%s' within session '%s'`, name, value, id );

            // Fetch the client
            const store = await this.getClient();

            // Always prepend 'session:' to every key, it's how we manage ACL in a
            // finely tuned manner:
            const write = await store.hSet( `session:${id}`, name, value );
            const expireAt = await store.hGet( `session:${id}`, 'expires' );
            const resetExpiry = await store.pExpireAt( `session:${id}`, expireAt );

            if( write && write.length && expireAt && resetExpiry )
                return Promise.resolve( true );
            else
                return Promise.resolve( false );
        }
        catch( exception ) {
            DEBUG( `[WRITE] Error: %o`, exception );

            return Promise.reject( `[REDIS_SESSION_STORE]: There was an issue writing the value '${value}' for field '${name}' in session '${id}' on the redis server: ${exception}. ` );
        }
    }


    /**
     * Method to delete a key and its hash from the redis server
     *
     * @param { string } id The session id of the key that the hash being deleted is named after
     *
     * @returns { Promise<boolean> } the promise for a `boolean`
     */
    public async delete( id: string ): Promise<boolean> {
        try {
            DEBUG( `Delete session '%s'`, id );

            // Fetch the client
            const store = await this.getClient();

            // Always prepend 'session:' to every key, it's how we manage ACL in a
            // finely tuned manner:
            const removed = await store.del( `session:${id}` );

            if( removed )
                return Promise.resolve( true );
            else
                return Promise.resolve( false );
        }
        catch( exception ) {
            DEBUG( `[DELETE] Error: %o`, exception );

            return Promise.reject( `[REDIS_SESSION_STORE]: There was an issue deleting the session '${id}' on the redis server: ${exception}. ` );
        }
    }


    /**
     * Creates a new client session
     *
     * @param { string } id The id of the session to create
     * @param { any } clientSession The object representation of a client session
     *
     * @returns { ClientSession|null } the promise for a {@link ClientSession} or `null`
     */
    public async createSession( id: string, sessionBits: SessionBits = {} as SessionBits ): Promise<ClientSession> {
        try {
            DEBUG( `Create session '%s'`, id );

            // When we want to create a session, first write the data:
            if( !await this.write( id, sessionBits ) )
                DEBUG( `There was an issue creating session '%s': the session could not be written to the redis server`, id );

            DEBUG( `Read session '%s' after write`, id );

            // Then return the read session, to ensure it was written, etc:
            const deferredRead = await this.read( id ) as ClientSession;

            //return this.store[id];
            return Promise.resolve( deferredRead );
        }
        catch( error ) {
            DEBUG( `[CREATE]: Error: %o`, error );

            return Promise.reject( error );
        }
    }


    /**
     * Returns an object from the memcached server representing a specific session
     *
     * @param { string | number } id The id of the session requested
     *
     * @returns { ClientSession } the promise for a {@link ClientSession}, or `null`
     */
    public async getSession( id: string ): Promise<ClientSession|null> {
        try {
            DEBUG( `Get session '%s'`, id );

            const deferredRead = await this.read( id );

            DEBUG( `Session found and read: ['%o']`, deferredRead );

            //return ( ( this.store.hasOwnProperty( id ) ) ? this.store[id] : false );
            return Promise.resolve( ( ( deferredRead ) ? deferredRead : null ) );
        }
        catch( error ) {
            DEBUG( `[GET]: Error: %o`, error );

            return Promise.reject( error );
        }
    };


    /**
     * Removes the specified session from memory
     *
     * @param { string } id The id of the session to delete
     *
     * @returns { boolean } the promise for a `boolean`
     */
    public async deleteSession( id: string ): Promise<boolean> {
        try{
            const found = await this.read( id );

            if( !found ) {
                DEBUG( `There was an issue deleting session '%s': The session could not be read`, id );

                return Promise.resolve( false );
            }

            DEBUG( `Delete session '%s'`, id );

            const deferredDelete = await this.delete( id );

            return Promise.resolve( deferredDelete );
        }
        catch( error ) {
            DEBUG( `[DELETE]: Error: %o`, error );

            return Promise.reject( error );
        }
    }


    /**
     * Gets a count of existing sessions. Since Redis does not support counting, 0 is always returned.
     *
     * @param void
     *
     * @returns { Promise<0> } the promise for the `number` `0`
     */
    public async countSessions(): Promise<any> {
        DEBUG( `[COUNT]: Cannot count sessions: Redis Session Store does not allow the counting of keys by design` );

        return Promise.resolve( 0 );
    }


    /**
     * Deletes any sessions which have gone beyond their expiration.
     *
     * **NOTE**: Since Redis expires its cache automatically by design, there is no need to implement
     * a functional `cleanSessions()`. However, because the session provider does manage session expiration
     * itself, we need to fool its interface by providing a method it can call.
     *
     * @param void
     *
     * @returns { Promise<void> } `void`
     */
    public async cleanSessions(): Promise<void> {
        DEBUG( `[CLEAN]: Cannot clean sessions: Redis cache expires automatically by design` );

        return Promise.resolve();
    }


    /**
     * Gets the specified value from the specified session
     *
     * @param { string | number } id The id of the session the value is being requested from
     * @param { string } name The name of the session value that is being requested
     * @param { any } defaultValue A default value in the event the property being requested is unknown or unset
     *
     * @returns { any }
     */
    public async get( id: string, name: string, defaultValue: any ): Promise<any> {
        try{
            DEBUG( `Get '%s' from session '%s'`, name, id );

            const deferredRead = await this.read( id ) as ClientSession;

            if( !deferredRead ) {
                DEBUG( `Error getting '%s' from session '%s'`, name, id );

                return Promise.reject( new Error( `[REDIS_SESSION_STORE] When getting '${name}', the session '${id}' could not be read. ` ) );
            }

            return Promise.resolve( _.get( deferredRead, name, null ) );
        }
        catch( error ) {
            DEBUG( `[DELETE]: Error: %o`, error );

            return Promise.reject( error );
        }
    }


    /**
     * Sets the specified value of the specified session
     *
     * @param { string: number } id The id of the session for which the value is being set
     * @param { string } name The name of the property for the specified session for which the value is being set
     * @param { any } value The value being set for the specified property of the specified session
     *
     * @returns { any }
     */
    public async set( id: string, name: string, value: any ): Promise<any> {
        try{
            DEBUG( `Set '%s' to '%o' for session '%s'`, name, value, id );

            const deferredRead = await this.read( id ) as ClientSession;

            if( !deferredRead ) {
                DEBUG( `Error setting '%s' to '%o' for session '%s'`, name, value, id );

                return Promise.reject( new Error( `[MEMCACHED_SESSION_STORE] When setting '${name}' to '${value}', the session '${id}' could not be found. ` ) );
            }

            const session = _.set( deferredRead, name, value );

            const deferredWrite = await this.write( id, session );

            if( !deferredWrite ) {
                DEBUG( `Error setting '%s' to '%o', for session '%s': session could not be stored with the memcached server`, name, value, id );

                return Promise.reject( new Error( `[MEMCHACED_SESSION_STORE] When setting '${name}' to '${value}', the session '${id}' could not be stored with the memcached server. ` ) );
            }

            return Promise.resolve( session[name] );
        }
        catch( error ) {
            DEBUG( `[DELETE]: Error: %o`, error );

            return Promise.reject( error );
        }
    }
}