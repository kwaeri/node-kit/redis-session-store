# Redis Environment via Docker-Compose

This readme covers important information about leveraging the docker compose file provided by this repository.

## The Easy Method

The easy method for running Redis in a container in order to support building and testing this module, involves leveraging the existing configuration packed in this repository, in order to configure the Redis container such that it accepts connections that don't originate within the container.

Here's how you do that:

```bash
docker run -d -v ./docker-compose/volumes/redis-environment/usr/local/etc:/usr/local/etc/redis --name myredis redis redis-server /usr/local/etc/redis/redis.conf
```

The relative path leads to the docker-compose directory within the repository. The command:

* Starts Redis with a configuration specified, which:
  * Binds Redis to all interfacs *(0.0.0.0)*
  * Sets a default password of **"default"** for the *"default"* user
  * Sets `protected-mode` to `no`.

This is the minimal configuration required for allowing connections to the Redis server from outside of the container (i.e. from a process running on the host, like our tests), in a way that allows for authentication. Please update the default configuration under the `conf` directory to reflect the proper username and password - should you choose to go this route.

This easy method comes after assuming that one could comment out the command and the volumes keys within the `docker-compose.yml` file in order to allow one to leverage redis without any password using the "default" user. Ideally this would have worked - but we want to #1 test authentication, and #2 an error is then received explaining that a) protected-mode is still on and preventing non-authenticated access, and b) connections from outside the host (container in this case) are not allowed.

The biggest issue to overcome is that Redis must be configured to bind to all interfaces, so that connections from outside the Redis host can be allowed - as our use case requires this.

In consideration of this, the "easy" method becomes what it is here, a simple command run from the root of the repository (as posted above). The "harder" method then becomes the [presumably] easiest method of all considering that it's a simple leveraging of a docker-compose configuration that simply requires we prepare a directory for volume mounting and copy some files; Modify the mount location specified within the compose configuration such that the first component of the volume mount specification is the full path to the repository directory containing redis.conf.

## The Harder Method

The preferred method involves leaving the `docker-compose.yml` exactly as-is. Instead, you'll copy the `redis-environment` directory under `docker-compose/volumes` to a location on the host where you will be running the redis container (i.e. to `/docker-volumes` on your host, specifically ).

*i.e. Terminal (Debian Linux)*:
```bash
sudo mkdir -p /docker-volumes
sudo copy -R ./docker-compose/volumes/redis-environment /docker-volumes/
```

Once you've copied the files over you should be sure to update `docker-compose.yml`, specifically the `volumes:` key's entry, so as to reflect the true local path (the first portion) you are mapping to the container filesystem (the second portion, i.e. `/usr/local/etc/redis`). If you performed the commands above, this isn't necessary - you only have to modify the path component in the event you want to use the files as they exist in their location within the repository, or from another location of your choosing (we all have a method to our mayhem).

Now you must choose to leverage ACLs or not, and if so - how to do so:

#### Without User ACLs

By default, the configuration simply sets bind to `0.0.0.0`, and `requirepass` to `default` all the same; It keeps example user configurations commented - as well as `aclfile`.

This means that by default, you'll be able to connect with a username of `default` and a password of `default`.

#### With User ACLs

If you would like to establish some users, the easiest - and recommended - method to do so involves uncommenting the `aclfile` entry within `redis.conf` and editing the `users.acl` file. Simply enter the appropriate username and passwords to suite your preferences in place of the existing ones within that file. I highly recommend to keep the configuration of those users as-is; they are appropriate for production environments with the exception that I would recommend removing the super-user altogether when deploying to production environments.

However you configure the users, make sure that all comment lines are removed as indicated within the file. Anything in the file other than specified users will cause the Redis instance to enter a restart-cycle. If you're not getting a connection, don't hesitate to `docker container logs redis-environment`.

**WARNING**: Please note that when I say *"appropriate"* - I exclusively mean for use with Nodekit Sessions when using the Redis Session Store; The commands and keys the limited users provided have privileges with are those exclusively used by the Redis Session Store. These would likely be entirely inappropriate for any other use-case.

## Using Docker Compose

This readme is not the appropriate place to learn how to use - or even install - docker compose. Historically, it was such a simple thing - with Docker configured and working there's a script you download and all that's necessary is to then configure your system to use it as a binary.

However, these days you have the option to use docker-compose as a standalone script, or as a part of docker-desktop (which is available for linux now). I choose to do the latter these days on my personal machine - but on service hosts I leverage the standalone plug-in install method with just the docker engine and cli.

To get set up, you can choose to follow these install instructions:

### Install Docker and Docker Compose

You have many options, please see the documentation linked about configuring [docker](https://gitlab.com/foss-contrib/gitlab-org/environments/gdk#install-docker) and [docker-compose](https://gitlab.com/foss-contrib/gitlab-org/environments/gdk#using-docker-compose) that exists on our GitLab Development Environment (don't follow instructions outside of docker and docker-compose headings; you're not configuring for GitLab Development - just installing Docker and Docker Compose ⇨ follow the links from those locations to get set up.)

However, Docker is fairly good about their documentation, I'd recommend going directly to them instead of leveraging a biased forum post somewhere on the internet (like mine, above) - and instead get set up how you'd prefer:

* [Docker Engine Installation](https://docs.docker.com/engine/install/) - Recommended for linux service hosts.
* [Docker Desktop (GA) for Linux Installation (Includes Docker Compose)](https://docs.docker.com/desktop/install/linux-install/) - I recommend these for your development workstations
* [Docker Desktop (GA) for Windows Installation (Includes Docker Compose)](https://docs.docker.com/desktop/install/windows-install/) - Your servers are hardly headless, go for it.
* [Docker Desktop (GA) for Mac Installation (Includes Docker Compose)](https://docs.docker.com/desktop/install/mac-install/) - Same as with Windows.
* [Stand-alone Docker Compose (docker-compose) Installation](https://github.com/docker/compose/tree/v2#linux) - From GitHub, v2, current but Linux Only; combine this with engine on linux hosts.
* [Stand-alone Docker Compose (docker compose) Installation](https://docs.docker.com/compose/install/other/) - From Docker Docs Site, v2, mostly current (refs GitHub), combine this with engine on linux hosts)
* [Stand-alone Docker Compose (docker-compose) Installation](https://stackoverflow.com/a/49839172/2041005) - From StackOverflow - v1/v2, lays out the old way and includes upgrading (avoid).
* [Stand-alone Docker Compose (local docker-compose) Installation ](https://github.com/docker/compose-cli) - From Github - the original v1, and obsolete (avoid).

### Running the Provided Compose Services

Once you're set up, traverse to the directory - in terminal/prompt - of the compose file, and run that magic command:

* If using docker-compose separately (installed following instructions from Docker or the GitHub repository for Docker Compose Plug-in):
  ```bash
  cd docker-compose
  docker-compose up -d
  ```
* If using Docker compose as provided by Docker Desktop:
  ```bash
  cd docker-compose
  docker compose up -d
  ```

Keeping in mind how you configured your Redis instance, you're now up and running.
