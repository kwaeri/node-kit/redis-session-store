FROM redis
COPY docker-compose/volumes/redis-environment/usr/local/etc/redis.conf /usr/local/etc/redis/redis.conf
CMD [ "redis-server", "/usr/local/etc/redis/redis.conf" ]