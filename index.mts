/**
 * SPDX-PackageName: kwaeri/redis-session-store
 * SPDX-PackageVersion: 1.0.3
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'


// INCLUDES


// ESM WRAPPER

export {
    RedisSessionStore
} from './src/redis-session-store.mjs';
